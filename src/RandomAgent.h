// RandomAgent.h

#ifndef RANDOM_AGENT_H
#define RANDOM_AGENT_H

#include "Action.h"
#include "Percept.h"
#include "Agent.h"

class RandomAgent: public Agent
{
public:
	RandomAgent();
	~RandomAgent();
	void Initialize();
	Action Process(Percept& percept);
	void GameOver(int score);
	Agent* clone();
};

#endif // RANDOM_AGENT_H
