// WorldRunner.cpp

#include <iostream>

#include "WorldRunner.h"

using namespace std;

bool WorldRunner::worldSet = false;
char* WorldRunner::worldFile = NULL;

int WorldRunner::runWorld(Agent* agent, int trials, bool headless, char* worldFilePath)
{
	WorldRunner::worldSet = true;
	WorldRunner::worldFile = worldFilePath;
	return runWorld(agent, trials, headless);
}

int WorldRunner::runWorld(Agent* agent, int trials, bool headless)
{
	Agent* agentCopy = agent->clone();
	
	WumpusWorld* wumpusWorld = NULL;
	Percept percept;
	Action action;
	int score;
	int trialScore;
	int totalScore = 0;
	float averageScore;
	int numMoves;
	
	for (int trial = 1; trial <= trials; trial++)
	{
		//cout << "TRIAL " << trial << endl;
		
		if (WorldRunner::worldSet) { wumpusWorld = new WumpusWorld(WorldRunner::worldFile); }
		else { wumpusWorld = new WumpusWorld(10); }
		//else { wumpusWorld = new WumpusWorld(4); }
		//wumpusWorld->Write (".world");
	
		trialScore = 0;
		
		wumpusWorld->Initialize();
		agent->Initialize();
		numMoves = 0;
		if (!headless) { cout << "Trial " << trial << endl << endl; }
		while ((!wumpusWorld->GameOver()) && (numMoves < MAX_MOVES_PER_GAME))
		{
			if (!headless) { wumpusWorld->Print(); }
			percept = wumpusWorld->GetPercept();
			action = agent->Process(percept);

			if (!headless) 
			{ 
				cout << "Action = "; 
				PrintAction (action); 
				cout << endl << endl; 
			}
			wumpusWorld->ExecuteAction (action);
			numMoves++;
		}
		score = wumpusWorld->GetScore();
		agent->GameOver(score);
		trialScore = trialScore + score;
			
		delete agent;
		delete wumpusWorld;
		agent = agentCopy->clone();
		
		totalScore = totalScore + trialScore;
	}
	averageScore = ((float) totalScore) / ((float) (trials));
	if (!headless)
	{
		cout << "All trials completed: Average score for all trials = " << averageScore << endl;
		cout << "Thanks for playing!" << endl << endl;
	}
	delete agentCopy;
	delete agent;
	
	return averageScore;
}
