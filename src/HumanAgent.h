// HumanAgent.h
// 
#ifndef HUMAN_AGENT_H
#define HUMAN_AGENT_H

#include "Action.h"
#include "Percept.h"
#include "Agent.h"

class HumanAgent: public Agent
{
public:
	HumanAgent();
	~HumanAgent();
	void Initialize();
	Action Process(Percept& percept);
	void GameOver(int score);
	Agent* clone();
};

#endif // HUMAN_AGENT_H
